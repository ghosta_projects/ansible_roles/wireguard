Wireguard
=========
This role will install wireguard on your server, also add wireguard clients to server and copy it's config files to ansible playbook directory

Platforms
------------
Ubuntu:     
  versions:   
    - jammy (22.04)

For ubuntu 20.04 working need to change handler from service reload to reset.
